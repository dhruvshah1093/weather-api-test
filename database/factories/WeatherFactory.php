<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Weather;
use Faker\Generator as Faker;

$factory->define(Weather::class, function (Faker $faker) {
    return [
        'date_recorded' =>  $faker->date($format = 'Y-m-d', $max = 'now'),
        'location_lat'  =>  $faker->latitude($min = -90, $max = 90),
        'location_lon'  =>  $faker->longitude($min = -180, $max = 180), 
        'state'		    =>	$faker->state,
        'city'			=>	$faker->city,
        'temperature'	=>  "28.5, 27.6, 26.7, 25.9, 25.3, 24.7, 24.3, 24.0, 27.1, 34.0, 38.6, 41.3, 43.2, 44.4, 45.0, 45.3, 45.1, 44.2, 41.9, 38.0, 35.0, 33.0, 31.1, 29.9",
        'highest'		=>  45.3,
        'lowest'	 	=>  24.0
    ];
});
