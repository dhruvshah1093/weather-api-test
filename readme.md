This project is ready to use, find all you need for the frontend in the "public/ressource" folder, and built your API respecting the question definition.

Basic controllers and models for this questions have been pre-generated.

If you encountered a permission error, just run sudo chmod -R 777. on the root folder of your project (projects/challenge)

You will be evaluated on the quality of your code, the result, the logic you implemented , and the functionalities you successfully implemented.

At first launch, the project will intialized to install the Laravel/Js environnement, it will take 3-5 minutes. 
When finish a file named "install.ready", will appear on the left menu at the root of the directory. 
Once you see this file you will be able to launch your app by clicking the button "Run"->Run on the topbar.

If you don't use any code migration to create the schema of your database, provide us a DUMP of the mysql database to be sure we can replicate your schema. Only your code (files in the challenge directory not included into the .gitignore files) is saved in this environnement, mysql data is wiped out after the test. 

You have root (sudo) access to this environnement.