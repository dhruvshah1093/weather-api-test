<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WeatherResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'id'        => $this->id,
            'date'      => $this->date_recorded,
            'location'  => [
                        'lat'   => $this->location_lat,
                        'lon'   => $this->location_lon,
                        'state' => $this->state,
                        'city'  => $this->city,
            ],
            'temperature'=> array_map('floatval',explode(',',$this->temperature)),            
        ];
    }
}
