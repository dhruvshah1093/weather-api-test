<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Weather;
use App\Http\Resources\WeatherResource;
use App\Http\Requests; 
use Validator;
use DB;

class WeatherController extends Controller
{
    /**
     * @param  Request
     * @return json
     */
    public function getWeather(Request $request){

        if($request->query('lat') || $request->query('lon'))
        {   
            $weather=Weather::where('location_lat',$request->query('lat'))
                            ->where('location_lon',$request->query('lon'))
                            ->get();
            if(count($weather) != 0){
                return WeatherResource::collection($weather)->response()->setStatusCode(200);
            }
            else{
                return response(null, 404);
            }
        }
        elseif($request->query('start') || $request->query('end'))
        {   
            $weather=Weather::whereBetween('date_recorded',[$request->query('start'),$request->query('end')])
                            ->get();
            if(count($weather) != 0){
                return WeatherResource::collection($weather)->response()->setStatusCode(200);
            }
            else{
                return response(null, 404);
            }
        }
        else{
            $weather=Weather::orderBy('id')->paginate(6);
            return WeatherResource::collection($weather)->response()->setStatusCode(200);
        }
    }

    /**
     * @param  Request
     * @return NUll
     */
    public function destroyAll(Request $request){

        //delete waether data by date range and location 
        if($request->query('start') && $request->query('end') && $request->query('lat') && $request->query('lon') )
        {
            $weather=Weather::whereBetween('date_recorded' ,[$request->query('start'),$request->query('end')])
                            ->where('location_lat' , $request->query('lat'))
                            ->where('location_lon' , $request->query('lon'))
                            ->delete();
            if($weather)
            {   
                return response(null,200);
            }
            
        }
        //delete weather data by id 
        if($request->query('id')){
            $weather=Weather::find($request->query('id'));
            if($weather == null){
                return response(null,404);
            }
            if($weather->delete())
            {
                return response(null,200);

            }
        }
        //
        if($request->query('start') && $request->query('end'))
        {
            $weather=Weather::whereBetween('date_recorded' ,[$request->query('start'),$request->query('end')])
                            ->delete();
            if($weather)
            {   
                return response(null,200);
            }
        }
        //delete all weather data
        if($request->path() == "api/erase")
        {
            $weather=Weather::truncate();

            if($weather)
            {
                return response(null,200);
            }
        }

        
    }


    /**
     * @param  Request
     * @return JSON
     */
    public function store(Request $request){


        //validation 
        $validator = Validator::make($request->all(), [
                'id'            => 'unique:weathers,id',
                'date'          => 'required',
                'temperature'   => 'required',
                'location'      => 'required'
            ]);

        if ($validator->fails()) {
            return response()->json([
                'errors'      => $validator->errors()
            ], 400);
        }
        $location=$request->input('location');
        $lat=$location['lat'];
        $lon=$location['lon'];
        $state=$location['state'];
        $city=$location['city'];
        //get array 
        $min_temp=min($request->input('temperature'));
        $max_temp=max($request->input('temperature'));
        //saving weather data
        $weather=($request->isMethod('put')) ? Weather::find($request->input('weather_id')) : new Weather();
        $weather->date_recorded=$request->input('date');
        $weather->temperature=implode(',',$request->input('temperature'));
        $weather->location_lat=$lat;
        $weather->location_lon=$lon;
        $weather->state=$state;
        $weather->city=$city;
        $weather->highest=$max_temp;
        $weather->lowest=$min_temp;
        if($weather->save())
        {
            return response(null,201);
        }

    }

    /**
     * @param  Request
     * @return json
     */
    public function getMinMax(Request $request){

        
        $minMax=DB::table('weathers')
                ->select('location_lat as lat','location_lon as lon','city','state',DB::raw('min(lowest) AS lowest'))
                ->whereBetween('date_recorded' ,[$request->query('start'),$request->query('end')])
                ->groupBy('city')
                ->orderBy('city')
                ->orderBy('state')
                ->get()
                ->toArray();
        if(count($minMax))
        {
            $minTempCities=DB::table('weathers')
                    ->select('city','state','location_lat as lat','location_lon as lon',DB::raw('max(highest) AS highest'))
                    ->whereBetween('date_recorded' ,[$request->query('start'),$request->query('end')])
                    ->groupBy('city')
                    ->orderBy('city')
                    ->orderBy('state')
                    ->get()
                    ->toArray();
            foreach($minMax as $key=>$mtc){
            $mtc->highest=$minTempCities[$key]->highest;
            }
            return response()->json($minMax,200);
        }
        else{
            return response()->json([
                'lat'=>'',
                'lon'=>'',
                'state'=> '',
                'city'=>'',
                'message'=>'There is no weather data in the given date range'
            ],200);
        }
        
    }

    /**
     * [getPreferedLocationInfo description]
     * @param  Request $request [description]
     * @return [json]           [description]
     */
    public function getPreferedLocationInfo(Request $request){

        $date=$request->query('date');
        $lat=$request->query('lat');
        $lon=$request->query('lon');
        $prefered=[];

        //get location of given lat and long
        $currentLocation=Weather::where('location_lat',$lat)
                                ->where('location_lon',$lon)
                                ->where('date_recorded',$date)
                                ->first();

        $prefLocation=Weather::select('location_lat as lat', 'location_lon as lon', 'city', 'state',DB::raw("Round( 6371 * acos(
                        cos(radians($lat)) * cos(radians(location_lat)) * cos(radians(location_lon) - radians($lon)) +
                        sin(radians($lat)) * sin(radians(location_lat))
                        ) ,0) AS distance"))
                        ->where('date_recorded',$date)
                        ->orderBy('distance','ASC')
                        ->orderBy('city')
                        ->orderBy('state')
                        ->get();
        //finding the median of temperature over next 3 days max
        foreach($prefLocation as $key=> $pref){
            $check=$this->isCurrentLocation($currentLocation, $pref, $date);

            if($check[0] == 2){
                $prefLocation->forget($key);
                continue;
            }
            //median fromula
            $temperature=$check[1];
            sort($temperature);
            $count=count($temperature);
            $middleval = floor(($count-1)/2);
            $low = $temperature[$middleval];
            $high = $temperature[$middleval+1];
            $median = round(($low+$high)/2,1);
            $pref->median_temperature=$median;
            array_push($prefered,$pref);
        }

        return response()->json($prefered,200);
    }

    public function isCurrentLocation($givenLocation,$preferLocation,$date){

        $check=0;
        $date_end=date('Y-m-d',strtotime($date." + 3 days"));
        $extTemp='';
        $temperature=Weather::select('temperature')
                    ->where('city',$preferLocation->city)
                    ->where('date_recorded','>',$date)
                    ->where('date_recorded', '<=', $date_end)
                    ->get();
        foreach($temperature as $temp){
            $extTemp=$extTemp.','.$temp->temperature;
        }
        $tempArray=array_map('floatval',explode(',',$extTemp));

        //check if given location is not null and 
        if($givenLocation != null){
            $givenTemp=array_map('floatval',explode(',',$givenLocation->temperature));
            if($givenLocation->state === $preferLocation->state){
                $check++;
            }
            for($i=0;$i<count($givenTemp);$i++){
                if(abs($givenTemp[$i]-$tempArray[$i]) > 20){
                    $check++;
                    break;
                }
            }
        }
        return [$check,$tempArray];
    }
}
