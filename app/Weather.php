<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
     protected $fillable = [
        'date', 'location_lat','location_lon','state','city','temperature','highest','lowest'
    ];

}
