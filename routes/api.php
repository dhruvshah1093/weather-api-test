<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Get all the weather data
Route::get('weather', 'WeatherController@getWeather');


//destroy all weather data and also destroy weather data based on on date range and location if there are parameters passed in url
Route::delete('erase', 'WeatherController@destroyAll');

//Create New weather data 
Route::post('weather','WeatherController@store');

Route::put('weather', 'WeatherController@store');
//Get highest and lowest temperature  with in a given date range 
Route::get('weather/temperature', 'WeatherController@getMinMax');


//Get Preferd Location Info 
Route::get('weather/locations' , 'WeatherController@getPreferedLocationInfo');

